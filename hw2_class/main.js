class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName (){
        return this.name;
    }
    getAge(){
        return this.age;
    }
    getSalary(){
        return this.salary;
    }
    setName(name){
      this.name = name;
    }
    setAge(age){
        this.age = age;
    }
    set(salary){
        this.salary=salary;
    }

}
class Programmer extends Employee {
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    getLang(){
        return this.lang;
    }
    setLang(lang){
        this.lang = lang;
    }
    getSalary() {
       return super.getSalary() * 3;
    }
}
const programmerOne = new Programmer("Alisa","20","20000","English");
const programmerTwo = new Programmer("Any","25","25000","Spain");

console.log(`ProgrammerOne : ${programmerOne.getName()}, ${programmerOne.getAge()}, ${programmerOne.getSalary()}, ${programmerOne.getLang()}`);

console.log(`ProgrammerTwo : ${programmerTwo.getName()}, ${programmerTwo.getAge()}, ${programmerTwo.getSalary()}, ${programmerTwo.getLang()}`);


